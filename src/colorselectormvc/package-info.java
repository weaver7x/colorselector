/**
 * <p>
 * Paquete que contiene la clase lanzadora y las constantes comunes a todas las
 * clases de la aplicación ColorSelectorMVC creada para la asignatura
 * Interacción Persona Computadora del Grado en Ingeniería Informática de
 * Sistemas en la Universidad de Valladolid.
 * </p>
 *
 * <p>
 * Se encarga de lanzar la aplicación correctamente y de mantener una
 * referencias a las constantes de la aplicación, comunes a todas las clases.
 * </p>
 *
 *
 * @author Cristian Tejedor García
 * @author José Ismael Taboada Rodero
 * @since 1.0
 * @version 1.0
 */
package colorselectormvc;