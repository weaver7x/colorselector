package colorselectormvc;

/**
 *
 * Constantes comunes a la aplicación.
 *
 * @author Cristian Tejedor García
 * @author José Ismael Taboada Rodero
 * @since 1.0
 * @version 1.0
 */
public final class Constants {

    /**
     * Valor máximo de una componente RGB.
     */
    public static final int RGB_MAX = 255;
    /**
     * Valor mínimo de una componente RGB.
     */
    public static final int RGB_MIN = 0;
    /**
     * Valor por defecto de una componente RGB.
     */
    public static final int RGB_DEF = RGB_MIN;
    /**
     * Máxima longitud de una componente RGB
     */
    public static final byte RGB_MAX_LENGTH = 3;
    /**
     * Máxima longitud de un numero hexadecimal
     */
    public static final byte HEX_MAX_LENGTH = 6;
    /**
     * Máxima longitud de una componente HSV
     */
    public static final byte HSV_MAX_LENGTH = 3;
    /**
     * Valor numérico mínimo en representación hexadecimal.
     */
    public static final char HEX_INT_MIN = '0';
    /**
     * Valor numérico máximo en representación hexadecimal.
     */
    public static final char HEX_INT_MAX = '9';
    /**
     * Valor de letra mínimo en representación hexadecimal.
     */
    public static final char HEX_CHAR_MIN = 'A';
    /**
     * Valor de letra máximo en representación hexadecimal.
     */
    public static final char HEX_CHAR_MAX = 'F';
    /**
     * Valor alfanumérico máximo hexadecimal.
     */
    public static final String HEX_MAX = "FFFFFF";
    /**
     * Valor alfanumérico mínimo hexadecimal.
     */
    public static final String HEX_MIN = "000000";
    /**
     * Valor alfanumérico por defecto hexadecimal.
     */
    public static final String HEX_DEF = HEX_MIN;
    /**
     * Valor numérico mínimo de la componente (H)ue de HSV.
     */
    public static final int HSV_H_MIN = 0;
    /**
     * Valor numérico máximo de la componente H de HSV.
     */
    public static final int HSV_H_MAX = 360;
    /**
     * Valor numérico por defecto de la componente H de HSV.*
     */
    public static final int HSV_H_DEF = HSV_H_MIN;
    /**
     * Valor numérico mínimo de las componentes S y V de HSV.
     */
    public static final int HSV_SV_MIN = 0;
    /**
     * Valor numérico máximo de las componentes S y V de HSV.
     */
    public static final int HSV_SV_MAX = 100;
    /**
     * Valor numérico por defecto de las componentes S y V de HSV.
     */
    public static final int HSV_SV_DEF = HSV_SV_MIN;
    /**
     * Valor de un slider por defecto.
     */
    public static final byte SLIDER_DEF = 0;
    /**
     * Mensaje de error para una componente RGB.
     */
    public static final String ERROR_MSG_RGB = "Min: " + RGB_MIN + " Max: " + RGB_MAX;
    /**
     * Mensaje de error para Hexadecimal.
     */
    public static final String ERROR_MSG_HEX = "Min: #" + HEX_MIN + " Max: #" + HEX_MAX;
    /**
     * Mensaje de error para la componente H de HSV.
     */
    public static final String ERROR_MSG_HSV_H = "Min: " + HSV_H_MIN + " Max: " + HSV_H_MAX;
    /**
     * Mensaje de error para las componentes S y V de HSV.
     */
    public static final String ERROR_MSG_HSV_SV = "Min: " + HSV_SV_MIN + " Max: " + HSV_SV_MAX;
    /**
     * Identificador que indica que no es ningun identificador valido.
     */
    public static final byte NO_ID = -1;
    /**
     * Identificador de la componente RED de RGB.
     */
    public static final byte ID_RGB_RED = 0;
    /**
     * Identificador de la componente GREEN de RGB.
     */
    public static final byte ID_RGB_GREEN = 1;
    /**
     * Identificador de la componente BLUE de RGB.
     */
    public static final byte ID_RGB_BLUE = 2;
    /**
     * Identificador de la representación en Hexadecimal.
     */
    public static final byte ID_HEX = 3;
    /**
     * Identificador de la componente H de HSV.
     */
    public static final byte ID_HSV_H = 4;
    /**
     * Identificador de la componente S de HSV.
     */
    public static final byte ID_HSV_S = 5;
    /**
     * Identificador de la componente V de HSV.
     */
    public static final byte ID_HSV_V = 6;
}