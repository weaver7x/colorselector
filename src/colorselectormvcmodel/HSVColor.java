package colorselectormvcmodel;

import colorselectormvc.Constants;
import java.awt.Color;

/**
 *
 * Modelo de la aplicación, color HSV.
 *
 * Mantiene un color HSV correcto con sus tres componentes.
 *
 *
 * @author Cristian Tejedor García
 * @author José Ismael Taboada Rodero
 * @since 1.0
 * @version 1.0
 *
 */
public class HSVColor {

    /**
     * Componente hue.
     */
    private int hue;
    /**
     * Componente saturación.
     */
    private int saturation;
    /**
     * Componente valor.
     */
    private int value;

    /**
     * Crea un nueva HSV con valores por defecto de un color HSV. Las tres
     * componentes sosn {@link Constants#RGB_DEF}
     */
    public HSVColor() {
        this.hue = Constants.HSV_H_DEF;
        this.saturation = Constants.HSV_SV_DEF;
        this.value = Constants.HSV_SV_DEF;
    }

    /**
     * Obtiene el valor HSV actual en formato RGB.
     *
     * @return Componentes (R)ed, (G)reen y (B)lue
     */
    public int[] hsvToRGB() {
        final float h = (360-(float) this.hue) / 360;
        final float s = (float) this.saturation / 100;
        final float v = (float) this.value / 100;
        final int rgb = Color.HSBtoRGB(h, s, v);
        int red = (rgb >> 16) & 0xFF;
        int blue = (rgb >> 8)& 0xFF;
        int green = rgb & 0xFF;
        return new int[]{red, green, blue};
    }

    /**
     * Devuelve la coordenada hue del HSV.
     *
     * @return Coordenada hue del HSV.
     */
    public int getH() {
        return hue;
    }

    /**
     * Devuelve la coordenada saturación del HSV.
     *
     * @return Coordenada saturación del HSV.
     */
    public int getS() {
        return saturation;
    }

    /**
     * Devuelve la coordenada valor del HSV.
     *
     * @return Coordenada valor del HSV.
     */
    public int getV() {
        return value;
    }

    /**
     * Establece un nuevo valor a la componente especificada.
     *
     * Si no es correcto lanza una {@link HSVColorException}.
     *
     * Los identificadores de las componentes pueden ser
     * {@link Constants#ID_HSV_H}, {@link Constants#ID_HSV_S} o
     * {@link Constants#ID_HSV_V}
     *
     * @param id Identificador de la componente.
     * @param newValue Nuevo valor a establecer.
     * @throws HSVColorException Si el nuevo valor es menor que
     * {@link Constants#HSV_H_MIN} o mayor que {@link Constants#HSV_H_MAX} en
     * caso de la variable hue, si el nuevo valor es menor que
     * {@link Constants#HSV_SV_MIN} o mayor que {@link Constants#HSV_SV_MAX} en
     * caso de la variable saturación o valor.
     */
    public void setNewValue(final byte id, final int newValue) throws HSVColorException {
        switch (id) {
            case Constants.ID_HSV_H:
                if (newValue < Constants.HSV_H_MIN || newValue > Constants.HSV_H_MAX) {
                    throw new HSVColorException(Constants.ERROR_MSG_HSV_H);
                }
                this.hue = newValue;
                break;
            case Constants.ID_HSV_S:
                if (newValue < Constants.HSV_SV_MIN || newValue > Constants.HSV_SV_MAX) {
                    throw new HSVColorException(Constants.ERROR_MSG_HSV_SV);
                }
                this.saturation = newValue;
                break;
            case Constants.ID_HSV_V:
                if (newValue < Constants.HSV_SV_MIN || newValue > Constants.HSV_SV_MAX) {
                    throw new HSVColorException(Constants.ERROR_MSG_HSV_SV);
                }
                this.value = newValue;
                break;
        }
    }
}