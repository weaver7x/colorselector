package colorselectormvcmodel;

import colorselectormvc.Constants;

/**
 *
 * Modelo de la aplicación, color Hexadecimal.
 *
 * Mantiene un color Hexadecimal correcto.
 *
 * @author Cristian Tejedor García
 * @author José Ismael Taboada Rodero
 * @since 1.0
 * @version 1.0
 *
 */
public class HexColor{

    /**
     * Valor hexadecimal del color persistente.
     */
    private String hex;

    /**
     * Constructor que incializa por defecto el color hexadecimal a
     * {@link Constants#HEX_DEF}
     */
    public HexColor() {
        this.hex = Constants.HEX_DEF;
    }

    /**
     * Obtiene el valor hexadecimal actual en formato RGB.
     *
     * @return Valor en RGB del color actual.
     */
    public int[] hexToRGB() {
        final byte HEX_RADIX = 16;
        return new int[]{
            Integer.valueOf(this.hex.substring(0, 2), HEX_RADIX),
            Integer.valueOf(this.hex.substring(2, 4), HEX_RADIX),
            Integer.valueOf(this.hex.substring(4, 6), HEX_RADIX)};
    }

    /**
     * Obtiene el valor hexadecimal actual.
     *
     * @return Valor en hexadecimal del color actual.
     */
    public String getHex() {
        return hex;
    }

    /**
     * Establece un nuevo valor hexadecimal, completando con 0's si es menor que
     * {@link Constants#HEX_MAX_LENGTH}
     *
     * @param newValue Nuevo valor a establecer.
     * @throws HexColorException En el caso de que no sea un numero hexadecimal
     * correcto.
     */
    public void setNewValue(final String newValue) throws HexColorException {
        if (isValid(newValue)) {
            this.hex = newValue;
            // Se comprueba que al menos tiene la longitud de {@link Constants#HEX_MAX_LENGTH}
            final int FILL_HEX = Constants.HEX_MAX_LENGTH - newValue.length();
            for (int i = 0; i < FILL_HEX; i++) {
                this.hex += '0';
            }
        } else {
            throw new HexColorException(Constants.ERROR_MSG_HEX);
        }
    }

    /**
     * Comprueba que un valor sea correcto en representacion hexadecimal.
     *
     * @param value Valor a comprobar.
     * @return TRUE si es correcto, FALSE en caso contrario.
     */
    public static boolean isValid(final String value) {
        final char[] chars = value.toCharArray();
        final int CHARS_SIZE = chars.length;
        int cont = 0;
        boolean isValid = true;
        char actualValue;
        while (cont < CHARS_SIZE && isValid) {
            actualValue = chars[cont];
            if ((actualValue < Constants.HEX_INT_MIN || actualValue > Constants.HEX_INT_MAX)
                    && (actualValue < Constants.HEX_CHAR_MIN || actualValue > Constants.HEX_CHAR_MAX)) {
                isValid = false;
            }
            cont++;
        }
        return isValid;
    }
}