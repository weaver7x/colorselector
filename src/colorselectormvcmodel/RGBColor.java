package colorselectormvcmodel;

import colorselectormvc.Constants;
import java.awt.Color;

/**
 *
 * Modelo de la aplicación, color RGB.
 *
 * Mantiene un color RGB correcto con sus tres componentes.
 *
 *
 * @author Cristian Tejedor García
 * @author José Ismael Taboada Rodero
 * @since 1.0
 * @version 1.0
 *
 */
public class RGBColor {

    /**
     * Componente roja.
     */
    private int red;
    /**
     * Componente verde.
     */
    private int green;
    /**
     * Componente azul.
     */
    private int blue;

    /**
     * Crea un nueva RGB con valores por defecto de un color RGB. Las tres
     * componentes son {@link Constants#RGB_DEF}
     */
    public RGBColor() {
        this.red = Constants.RGB_DEF;
        this.green = Constants.RGB_DEF;
        this.blue = Constants.RGB_DEF;
    }

    /**
     * Obtiene el valor RGB actual en formato Hexadecimal.
     *
     * @return valor Hexadecimal.
     */
    public String rgbToHex() {
        return "" + toHexValue(getRed()) + toHexValue(getGreen()) + toHexValue(getBlue());
    }

    /**
     * Convierte un numero de una componente RGB a hexadecimal.
     *
     * @param number Componente a convertir a hexadecimal.
     * @return Valor hexadecimal de la componente RGB.
     */
    private static String toHexValue(final int number) {
        final StringBuilder builder =
                new StringBuilder(Integer.toHexString(number & 0xff));
        while (builder.length() < 2) {
            builder.insert(0, '0');
        }
        return builder.toString()
                .toUpperCase();
    }

    /**
     * Obtiene el valor RGB actual en formato HSV.
     *
     * @return Coordenadas H, S y V.
     */
    public int[] rgbToHSV() {
        float hsv[] = new float[3];
        Color.RGBtoHSB(this.red, this.blue, this.green, hsv);
        final int h = (int) Math.round(360 - hsv[0] * 360);
        final int s = (int) Math.round(hsv[1] * 100);
        final int v = (int) Math.round(hsv[2] * 100);
        return new int[]{h, s, v};
    }

    /**
     * Devuelve la coordenada roja del RGB.
     *
     * @return Coordenada roja del RGB.
     */
    public int getRed() {
        return red;
    }

    /**
     * Devuelve la coordenada verde del RGB.
     *
     * @return Coordenada verde del RGB.
     */
    public int getGreen() {
        return green;
    }

    /**
     * Devuelve la coordenada azul del RGB.
     *
     * @return Coordenada azul del RGB.
     */
    public int getBlue() {
        return blue;
    }

    /**
     * Establece un nuevo valor a la componente especificada.
     *
     * Si no es correcto lanza una {@link RGBColorException}.
     *
     * Los identificadores de las componentes pueden ser
     * {@link Constants#ID_RGB_RED}, {@link Constants#ID_RGB_GREEN} o
     * {@link Constants#ID_RGB_BLUE}
     *
     * @param id Identificador de la componente.
     * @param newValue Nuevo valor a establecer.
     * @throws RGBColorException Si el nuevo valor es menor que
     * {@link Constants#RGB_MIN} o mayor que {@link Constants#RGB_MAX}
     */
    public void setNewValue(final byte id, final int newValue) throws RGBColorException {
        if (newValue < Constants.RGB_MIN || newValue > Constants.RGB_MAX) {
            throw new RGBColorException(Constants.ERROR_MSG_RGB);
        }
        switch (id) {
            case Constants.ID_RGB_RED:
                this.red = newValue;
                break;
            case Constants.ID_RGB_GREEN:
                this.green = newValue;
                break;
            case Constants.ID_RGB_BLUE:
                this.blue = newValue;
                break;
        }
    }
}