/**
 * <p>
 * Paquete que contiene los modelos de la aplicacion ColorSelectorMVC creada
 * para la asignatura Interacción Persona Computadora del Grado en Ingeniería
 * Informática de Sistemas en la Universidad de Valladolid.
 * </p>
 *
 * <p>
 * Se han considerado los modelos de color: RGB, HSV y hexadecimal.
 * <b>RGB</b>: Red - Green - Blue.
 * <b>HSV</b>: Hue - Saturation - Value.
 * <b>HEX</b>: Representacion hexadecimal.
 * </p>
 *
 * <p>
 * Se mantienen en memoria con valores coherentes, ya que disponen de sus
 * propias clases de excepción.
 * </p>
 *
 *
 * @author Cristian Tejedor García
 * @author José Ismael Taboada Rodero
 * @since 1.0
 * @version 1.0
 */
package colorselectormvcmodel;