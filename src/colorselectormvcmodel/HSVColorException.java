package colorselectormvcmodel;

/**
 * Excepción especifica de la clase {@link HSVColor}.
 *
 * @author Cristian Tejedor García
 * @author José Ismael Taboada Rodero
 * @since 1.0
 * @version 1.0
 */
public final class HSVColorException extends Exception {

    /**
     * Constructor con mensaje.
     *
     * @param msg Mensaje a mostrar.
     */
    public HSVColorException(final String msg) {
        super(msg);
    }
}