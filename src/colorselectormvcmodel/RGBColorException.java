package colorselectormvcmodel;

/**
 * Excepcion especifica de la clase {@link RGBColor}.
 *
 * @author Cristian Tejedor García
 * @author José Ismael Taboada Rodero
 * @since 1.0
 * @version 1.0
 */
public final class RGBColorException extends Exception {

    /**
     * Constructor con mensaje.
     *
     * @param msg Mensaje a mostrar.
     */
    public RGBColorException(final String msg) {
        super(msg);
    }
}