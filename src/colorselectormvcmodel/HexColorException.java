package colorselectormvcmodel;

/**
 * Excepción específica de la clase {@link HexColor}.
 *
 * @author Cristian Tejedor García
 * @author José Ismael Taboada Rodero
 * @since 1.0
 * @version 1.0
 */
public final class HexColorException extends Exception {

    /**
     * Constructor con mensaje.
     *
     * @param msg Mensaje a mostrar.
     */
    public HexColorException(final String msg) {
        super(msg);
    }
}