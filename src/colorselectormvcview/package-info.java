/**
 * <p>
 * Paquete que contiene la interfaz gráfica de la aplicación ColorSelectorMVC
 * creada para la asignatura Interacción Persona Computadora del Grado en
 * Ingeniería Informática de Sistemas en la Universidad de Valladolid.
 * </p>
 *
 * <p>
 * La interfaz grafica esta diseñada con el editor gráfico de Java Swing en
 * NetBeans.
 * </p>
 *
 *
 * @author Cristian Tejedor García
 * @author José Ismael Taboada Rodero
 * @since 1.0
 * @version 1.0
 */
package colorselectormvcview;