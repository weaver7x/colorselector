/**
 * <p>
 * Paquete que contiene las imágenes utilizadas en la aplicación
 * ColorSelectorMVC creada para la asignatura Interacción Persona Computadora
 * del Grado en Ingeniería Informática de Sistemas en la Universidad de
 * Valladolid.
 * </p>
 *
 * <p>
 * Las imágenes serán utilizadas para mostrar ejemplos en la informacion de
 * ayuda.
 * </p>
 *
 *
 * @author Cristian Tejedor García
 * @author José Ismael Taboada Rodero
 * @since 1.0
 * @version 1.0
 */
package colorselectormvcsource;