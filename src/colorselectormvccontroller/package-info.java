/**
 * <p>
 * Paquete que contiene el controlador de la aplicación ColorSelectorMVC creada
 * para la asignatura Interacción Persona Computadora del Grado en Ingeniería
 * Informática de Sistemas en la Universidad de Valladolid.
 * </p>
 *
 * <p>
 * Se encarga de la lógica de negocio de la aplicación, recibiendo los eventos
 * de la vista y actualizaándola correctamente.
 * También se encarga de mantener la coherencia entre los modelos.
 * </p>
 *
 *
 * @author Cristian Tejedor García
 * @author José Ismael Taboada Rodero
 * @since 1.0
 * @version 1.0
 */
package colorselectormvccontroller;