package colorselectormvccontroller;

import colorselectormvc.Constants;
import colorselectormvcmodel.HSVColor;
import colorselectormvcmodel.HSVColorException;
import colorselectormvcmodel.HexColor;
import colorselectormvcmodel.HexColorException;
import colorselectormvcmodel.RGBColor;
import colorselectormvcmodel.RGBColorException;
import colorselectormvcview.ColorSelectorView;

/**
 *
 * Controlador de la aplicación.
 *
 *
 * Mantiene la coherencia entre los distintos modelos de la aplicación.
 *
 * Avisa a la vista de cuándo debe actualizarse.
 *
 * Recibe notificaciones de la vista.
 *
 * Cambia los valores de * los modelos de color, manteniendo la coherencia entre
 * ellos, de tal manera que todas las codificaciones represente en mismo color.
 *
 * @author Cristian Tejedor García
 * @author José Ismael Taboada Rodero
 * @since 1.0
 * @version 1.0
 */
public class ColorSelectorController {

    /**
     * Vista que actualizará.
     */
    private final ColorSelectorView view;
    /**
     * Modelo RGB.
     */
    private final RGBColor rgbModel;
    /**
     * Modelo HEX.
     */
    private final HexColor hexModel;
    /**
     * Modelo HSV.
     */
    private final HSVColor hsvModel;
    /**
     * Constante que indica que hubo un problema al obtener el valor RGB o HSV.
     */
    private final static int RGB_HSV_DEFAULT = -1;
    /**
     * Constante que indica que no habia cadena a extraer de un RGB o HSV.
     */
    private final static int RGB_HSV_NO_TEXT = -2;

    /**
     * Constructor que inicializa la vista y sus modelos.
     *
     * @param view Vista a actualizar.
     * @param rgbModel Modelo RGB a mantener coherente.
     * @param hexModel Modelo HEX a mantener coherente.
     * @param hsvModel Modelo HSV a mantener coherente.
     */
    public ColorSelectorController(final ColorSelectorView view, final RGBColor rgbModel, final HexColor hexModel, final HSVColor hsvModel) {
        this.view = view;
        this.rgbModel = rgbModel;
        this.hexModel = hexModel;
        this.hsvModel = hsvModel;
    }

    /**
     * Actualiza la vista de la aplicación. Mantiene coherencia en todos los
     * modelos de la aplicación.
     *
     * @param id Aquel campo de la vista que ha cambiado.
     */
    public void update(final byte id) {
        switch (id) {
            case Constants.ID_RGB_RED:
            case Constants.ID_RGB_GREEN:
            case Constants.ID_RGB_BLUE:
                final int valueRGB = rgbHsvFieldControl(this.view.getValueField(id));
                this.view.showError(id, valueRGB == RGB_HSV_DEFAULT ? Constants.ERROR_MSG_RGB : "");
                if (valueRGB != RGB_HSV_DEFAULT && valueRGB != RGB_HSV_NO_TEXT) {
                    // Se establece en el modelo el nuevo valor.
                    // Si no es correcto no se establece ni se muestran cambios.
                    try {
                        this.rgbModel.setNewValue(id, valueRGB);
                        toOthers(id);
                    } catch (final RGBColorException ex) {
                        this.view.showError(id, Constants.ERROR_MSG_RGB);
                    }
                }
                break;

            case Constants.ID_HEX:
                final String valueHex = this.view.getValueField(id);
                try {
                    this.hexModel.setNewValue(valueHex);
                    toOthers(id);
                } catch (final HexColorException exc) {
                    // No puede tener error porque la vista lo impide.
                }
                break;

            case Constants.ID_HSV_H:
            case Constants.ID_HSV_S:
            case Constants.ID_HSV_V:
                final int valueHSV = rgbHsvFieldControl(this.view.getValueField(id));
                this.view.showError(id, valueHSV == RGB_HSV_DEFAULT ? id == Constants.ID_HSV_H ? Constants.ERROR_MSG_HSV_H : Constants.ERROR_MSG_HSV_SV : "");
                if (valueHSV != RGB_HSV_DEFAULT && valueHSV != RGB_HSV_NO_TEXT) {
                    // Se establece en el modelo el nuevo valor.
                    // Si no es correcto no se establece ni se muestran cambios.
                    try {
                        this.hsvModel.setNewValue(id, valueHSV);
                        toOthers(id);
                    } catch (final HSVColorException ex) {
                        this.view.showError(id, id == Constants.ID_HSV_H ? Constants.ERROR_MSG_HSV_H : Constants.ERROR_MSG_HSV_SV);
                    }
                }
                break;
        }
    }

    /**
     * Establece valores en los demás modelos de la aplicación para que haya
     * consistencia y muestra el nuevo valor en todos los formatos de color.
     *
     * @param id Aquel campo de la vista que ha cambiado y quiere propagar su
     * cambio a los demás.
     */
    private void toOthers(final byte id) {
        boolean drawColor = false;
        boolean updateRGB = false;
        boolean updateHex = false;
        boolean updateHSV = false;
        switch (id) {
            case Constants.ID_RGB_RED:
            case Constants.ID_RGB_GREEN:
            case Constants.ID_RGB_BLUE:
                updateHex = true;
                updateHSV = true;
                updateRGB = false;
                drawColor = true;
                break;

            case Constants.ID_HEX:
                updateRGB = true;
                updateHSV = true;
                updateHex = false;
                drawColor = true;
                break;

            case Constants.ID_HSV_H:
            case Constants.ID_HSV_S:
            case Constants.ID_HSV_V:
                updateRGB = true;
                updateHex = true;
                updateHSV = false;
                drawColor = true;
                break;
        }
        try {
            // Siempre el primero RGB
            if (updateRGB) {
                // RGB
                // Establece valores
                final int[] rgb = this.hsvModel.hsvToRGB();
                // Establece valores
                final byte[] rgbComponents = new byte[]{Constants.ID_RGB_RED,
                    Constants.ID_RGB_GREEN, Constants.ID_RGB_BLUE};
                final int RGB_LENGTH = rgbComponents.length;
                for (byte i = 0; i < RGB_LENGTH; i++) {
                    this.rgbModel.setNewValue(rgbComponents[i], rgb[i]);
                }
                // Se elimina posible mensaje de error previo
                this.view.showError(Constants.ID_RGB_RED, "");
                this.view.showError(Constants.ID_RGB_GREEN, "");
                this.view.showError(Constants.ID_RGB_BLUE, "");
                // Se muestran valores
                // Siempre se muestra una vez que se haya hecho la persistencia.
                this.view.showRGB();
            }
            if (updateHex) {
                // Establece valores
                this.hexModel.setNewValue(this.rgbModel.rgbToHex());
                // Se elimina posible mensaje de error previo
                this.view.showError(Constants.ID_HEX, "");
                // Se muestran valores
                // Siempre se muestra una vez que se haya hecho la persistencia.
                this.view.showHex();
            }
            if (updateHSV) {
                // HSV
                final int[] hsv = this.rgbModel.rgbToHSV();
                // Establece valores
                final byte[] hsvComponents = new byte[]{Constants.ID_HSV_H,
                    Constants.ID_HSV_S, Constants.ID_HSV_V};
                final int HSV_LENGTH = hsvComponents.length;
                for (byte i = 0; i < HSV_LENGTH; i++) {
                    this.hsvModel.setNewValue(hsvComponents[i], hsv[i]);
                }
                // Se elimina posible mensaje de error previo
                this.view.showError(Constants.ID_HSV_H, "");
                this.view.showError(Constants.ID_HSV_S, "");
                this.view.showError(Constants.ID_HSV_V, "");
                // Se muestran valores
                // Siempre se muestra una vez que se haya hecho la persistencia.
                this.view.showHSV();
            }

            // Al final siempre de {@link ColorSelectorController#toOther()}
            if (drawColor) {
                // Pintar el color
                this.view.drawColorPanel();
            }
        } catch (final RGBColorException | HexColorException | HSVColorException ex) {
        }
    }

    /**
     * Obtiene un número entero de una cadena de caracteres.
     *
     * @param text Campo de texto a comprobar.
     * @return {@link ColorSelectorController#RGB_HSV_DEFAULT} si no es un
     * número entero, {@link ColorSelectorController#RGB_HSV_NO_TEXT} si esta
     * vacio o número entero parseado en caso contrario.
     */
    private int rgbHsvFieldControl(final String text) {
        int rgbValue;
        if (text == null || text.isEmpty()) {
            rgbValue = RGB_HSV_NO_TEXT;
        } else {
            try {
                rgbValue = Integer.parseInt(text);
            } catch (final NumberFormatException nFormatExc) {
                rgbValue = RGB_HSV_DEFAULT;
            }
        }
        return rgbValue;
    }
}